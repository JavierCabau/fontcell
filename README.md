 FOntCell

**FOntCell** is a software module in Python for automatic computed fusion of ontologies. 
**FOntCell** produces the results of the merged ontology in OBO format that can be iteratively reused by FOntCell to adapt continuously the ontologies with the new data, such of the Human Cell Atlas, endlessly produced by data-driven classification methods. To navigate easily across the fused ontologies, it generates HTML files with tabulated and graphic summaries, and an interactive circular Directed Acyclic Graphs of the merged results.

# Prerequisites

- Python3

- pip3

It is necessary to use pip3 to download **FOntCell** and use it with python3. 
To install pip3 over Python3 use the command:

`sudo apt-get install python3-pip`

- TKinter 

Tkinter library is a dependency that will not be installed during **FOntCell** installation. If TKinter has not been previously installed, an error will occur during **FOntCell** import. To install TKinter over Python3 use the command:

`sudo apt-get install python3-tk`

- mpi.h

Mpi.h is a mpi4py dependency that might not have been installed during mpi4py installation, and if it is had not been installed, an error might occur. To install mpi.h use the command:

`sudo apt install libopenmpi-dev`

# Instructions to download FOntCell

**FOntCell** module is available at PyPI and can be installed using the command:

`sudo pip3 install FOntCell`

# User manual of FOntCell:

After downloading and installing **FOntCell** using pip3, the user has to create the following directories:

- output_folder

Is the directory where FOntCell will output the results: 
    • Fused ontology file
    • html files
    • Figures

- input_folder

The input_folder should contain a Configuration file, the two ontologies, and the ontology_edit documents, that should be placed there by the user.

# Files required in the Input directory

- Configuration file

The configuration file should have the following arguments in .txt format.

_Most arguments will be for ontology 1 (A) and ontology 2 (B)_

| Arguments | Description |
| ------ | ------ |
| input_folder | _str_, path to input directory |
| output_folder | _str_, path to output directory | 
| parallelization | _bool_, if True, perform parallelization*| 
| proc | _int_, number of processors¹*|
| parse_ontology1 | _bool_, if True, perform parsing of ontology1|
| parse_ontology2 | _bool_, if True, perform parsing of ontology2|
| file1 | _str_, path to ontology1²³|
| file1 | _str_, path to ontology2²³|
| take_synonyms1 | _bool_, if True, take also the synonyms from ontology1 classes³|
| take_synonyms2 | _bool_, if True, take also the synonyms from ontology2 classes³|
| ontologyName1 | _str_, name of ontology1³|
| ontologyName2 | _str_, name of ontology2³|
| synonym_type1 | _str_, _one or more_, ontology1 arguments specifying the location of the labels of the synonyms³|
| synonym_type2 | _str_, _one or more_, ontology2 arguments specifying the location of the labels of the synonyms³|
| label_type1 | _str_, _one or more_, ontology1 arguments specifying the location of the labels of the classes³|
| label_type2 | _str_, _one or more_, ontology2 arguments specifying the location of the labels of the classes³|
| relative_type1 | _str_, _one or more_, ontology1 arguments specifying the location of the IDs of the ascendants³|
| relative_type2 | _str_, _one or more_, ontology2 arguments specifying the location of the IDs of the ascendants³|
| file_clean_ontology1 | _str_, name of the script file with the instructions to **FOntCell** for the automatic editing of ontology1²³|
| file_clean_ontology2 | _str_, name of the script file with the instructions to **FOntCell** for the automatic editing of ontology2²³|
| del_old_trials_files | _bool_, if True, deletes old files. **Recommended for every new use of FOntCell** |
| onto_fuse_classes1 | _bool_, if true, fuse the classes in ontology1 if they have the same label|
| onto_fuse_classes2 | _bool_, if true, fuse the classes in ontology2 if they have the same label|
| onto_restriction1 | _bool_, if true, fuse the classes in ontology1 if they have the same ID (or secondary info) and the same label|
| onto_restriction2 | _bool_, if true, fuse the classes in ontology2 if they have the same ID (or secondary info) and the same label|
| onto_list_clear1 | _str_, _one or more_, delete the introduced words from labels of ontology1⁴|
| onto_list_clear2 | _str_, _one or more_, delete the introduced words from labels of ontology2⁴|
| semantical | _bool_, if true, perform the synonyms with label name matching|
| text_process | _bool_, if true, perform test processing|
| split_from1 | _str_, split and take the labels of ontology1 from the introduced word to end⁴|
| split_from2 | _str_, split and take the labels of ontology2 from the introduced word to end⁴|
| split_since1 | _str_, split and take the labels of ontology1 from the beginning since the word introduced⁴|
| split_since2 | _str_, split and take the labels of ontology2 from the beginning since the word introduced⁴|
| windowsize | _int_, size of the convolutional window (in edges)|
| namethreshold | _float_, threshold between 0.0-1.0 for name mapping|
| localnamethreshold | _float_, threshold between 0.0-1.0 for local name mapping (used in structure mapping)|
| structure_threshold | _float_ threshold between 0.0-1.0 for structure mapping*|
| structure_method| _str_ structure matching type: 'blondel', 'cosine', 'euclidean', 'pearson' or 'constraint-based'|
| automatic | _bool_ if true run FOntCell automatic (unsupervished), if false run FOntCell semi-automatic, user control at structure alignment |

_* Argument can be blank if your analysis does not require those arguments_

_¹ only if parallelization is True_

_² files must be at input directory_

_³ only if parse ontology is True_

_⁴ if text_process is True_

_Every argument of the configuration.txt file needs to be precede by a '>' character forward to be parsed. The user do not have to introduce a threshold for a test that it has not been selected_

- Ontology edition File

This documents allows the user to edit the ontology after parse in order to direct the ontology fusion.

The document must be in .txt file with the following instructions defining how to automatic edit the ontology file:

|**INstruction** | *Syntax** |  **Example** | **Description** |
| ------ | ------ | ------ | ------ |
| delete class | '...' | 'ontology label x' | deletes the nodes/classes that contais the label  |
| concatenate classes | [[...], [...]]   | [[[ontology_class_synonyms]1, ID:0], [[ontology_class_synonyms2], ID:1]] | concatenate two classes, allows introduce new classes |
| delete class from ID | ID: ' ... '| ID:'xxxxx' | delete the class that has the ID introduced|
| fuse classes | f[[...], [...]] | f[[ontology_synonyms1], [ontology_synonyms2]] | fuse two classes. The resultant class conserves the ID from the first class, and the descendant-ascendant relations)|

- Ontology Files

The ontologies to be fused can be in .owl format (that requires a parse) or in a .ods format, if one wishes to save the parse step.

The .ods file has the graph-edge information in two columns as in the following example:

| Ascendant class | Descendant class  |
| ------ | ------ |
| [[class synonyms 1], ID class 1] | [[class synonyms 2], ID class 2] |
| [[class synonyms 1], ID class 1] | [[class synonyms 3], ID class 3] |
| [[class synonyms 2], ID class 2] | [[class synonyms 4], ID class 4] | 

_The first column corresponds to 'parent' and the second column to the 'descendant'. The possible synonyms of each class are separated by commas._

# Files created in the Output directory after fusion

- .owl file

This document is the resultant ontology from the fusion of two ontologies. The structure is the same as of ontology 1 (A), with the new classes added from ontology 2 (B) at the top of ontology class section.

- FOntCell_OntolgyA_OntologyB.html file

This file contains information about the fusion. First, one can see three interactive circular graphs: ontology A, ontology B and the merged ontology. Additionally, it contains other information such as: the different thresholds values and statistics about the fusion. The file also shows a direct link to the OBO-format merged ontology, and a representative image of type of node assignation between ontology A and B (a donut graph) and an image of an Euler-Venn diagram (using squares) about how the fusion has worked.

- .html of DAGs and .png of figures files

These files are the files encrusted in the FOntCell_OntologyA_OntologyB.html file. The .html files are all the different interactive circular graphs, and the .png files contain the Euler-Venn diagram and the donut graph. 

# Troubleshooting

For parallel computation **FOntCell** requires bigmpi4py (https://www.biorxiv.org/content/10.1101/517441v1) 

If bigmpi4py has been installed in a different conda environment from the one of the **FOntCell** installation, the parallelization will not work well (all the processes will run on a single processor). In this case, **FOntCell** will work but without parallelization.

Running **FOntCell** will raise a problem if Graphviz has not been properly installed. For a correct graphviz installation, one can use the command:ation you can use the command:

`sudo apt-get install graphviz`

# Example of use of how to run FOntCell

Open python3 in bash:

`sudo python3`

Import the **FOntCell** module:

`import FOntCell`

After the **FOntCell** module is imported, one can use the following functions:

To run the fusion of two onthologies:

`FOntCell.run('path/to/configuration_file.txt')` 

To run the demo of the fusion of CELDA with Lifemap

`FOntCell.run_demo()` 

To clean internal files from old runs (recommended for use before a new fusion, especially if one of the ontologies from a previous fusion will be used again)

`FOntCell.clean()` 
